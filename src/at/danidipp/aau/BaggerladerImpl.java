package at.danidipp.aau;

public class BaggerladerImpl extends BaumaschineImpl implements Bagger, Lader {
    private double grabtiefe;
    private double reichweite;
    public double schaufelvolumen;
    public double kipphoehe;

    public BaggerladerImpl(String name, int leistung, double geschwindigkeit, double gewicht, double grabtiefe, double reichweite, double schaufelvolumen, double kipphoehe) {
        super(name, leistung, geschwindigkeit, gewicht);
        this.grabtiefe = grabtiefe;
        this.reichweite = reichweite;
        this.schaufelvolumen = schaufelvolumen;
        this.kipphoehe = kipphoehe;
    }

    @Override
    public double getGrabtiefe() {
        return grabtiefe;
    }

    @Override
    public void setGrabtiefe(double grabtiefe) {
        if (grabtiefe > MAX_GRABTIEFE){
            this.grabtiefe = MAX_GRABTIEFE;
            System.err.println("Maximale Grabtiefe von "+MAX_GRABTIEFE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.grabtiefe = grabtiefe;
    }


    @Override
    public double getReichweite() {
        return reichweite;
    }

    @Override
    public void setReichweite(double reichweite) {
        if (reichweite > MAX_REICHWEITE){
            this.reichweite = MAX_REICHWEITE;
            System.err.println("Maximale Reichweite von "+MAX_REICHWEITE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.reichweite = reichweite;
    }

    @Override
    public double getSchaufelvolumen() {
        return schaufelvolumen;
    }

    @Override
    public void setSchaufelvolumen(double schaufelvolumen) {
        if (schaufelvolumen > MAX_SCHAUFELVOLUMEN){
            this.schaufelvolumen = MAX_SCHAUFELVOLUMEN;
            System.err.println("Maximales Schaufelvolumen von "+MAX_SCHAUFELVOLUMEN+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.schaufelvolumen = schaufelvolumen;
    }

    @Override
    public double getKipphoehe() {
        return kipphoehe;
    }

    @Override
    public void setKipphoehe(double kipphoehe) {
        if (kipphoehe > MAX_KIPPHOEHE){
            this.kipphoehe = MAX_KIPPHOEHE;
            System.err.println("Maximale Kipphöhe von "+MAX_KIPPHOEHE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.kipphoehe = kipphoehe;
    }

    @Override
    public void druckeBeschreibung() {
        super.druckeBeschreibung();
        System.out.println("Grabtiefe: "+grabtiefe);
        System.out.println("Reichweite: "+reichweite);
        System.out.println("Schaufelvolumen: "+schaufelvolumen);
        System.out.println("Kipphoehe: "+kipphoehe);
    }
}
