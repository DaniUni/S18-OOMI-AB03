package at.danidipp.aau;

public interface Lader extends Baumaschine {
    public final double MAX_SCHAUFELVOLUMEN = 10;
    public final double MAX_KIPPHOEHE = 5;

    public double getSchaufelvolumen();
    public void setSchaufelvolumen(double schaufelvolumen);
    public double getKipphoehe();
    public void setKipphoehe(double kipphoehe);
    public void druckeBeschreibung();
}
