package at.danidipp.aau;

public class Main {

    public static void main(String[] args) {

        BaggerImpl b1 = new BaggerImpl("b1", 600, 40, 2000, 7, 4);
        b1.druckeBeschreibung();
        System.out.println();

        LaderImpl l1 = new LaderImpl("l1", 300, 80, 1700, 400 ,3);
        l1.druckeBeschreibung();
        System.out.println();

        BaggerladerImpl bl1 = new BaggerladerImpl("bl1", 550, 50, 1200, 2, 4, 300, 2);
        bl1.druckeBeschreibung();
    }
}
