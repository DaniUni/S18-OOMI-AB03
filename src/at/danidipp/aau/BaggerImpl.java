package at.danidipp.aau;

public class BaggerImpl extends BaumaschineImpl implements Bagger {
    private double grabtiefe;
    private double reichweite;

    public BaggerImpl(String name, int leistung, double geschwindigkeit, double gewicht, double grabtiefe, double reichweite) {
        super(name, leistung, geschwindigkeit, gewicht);
        this.grabtiefe = grabtiefe;
        this.reichweite = reichweite;
    }

    @Override
    public double getGrabtiefe() {
        return grabtiefe;
    }

    @Override
    public void setGrabtiefe(double grabtiefe) {
        if (grabtiefe > MAX_GRABTIEFE){
            this.grabtiefe = MAX_GRABTIEFE;
            System.err.println("Maximale Grabtiefe von "+MAX_GRABTIEFE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.grabtiefe = grabtiefe;
    }

    @Override
    public double getReichweite() {
        return reichweite;
    }

    @Override
    public void setReichweite(double reichweite) {
        if (reichweite > MAX_REICHWEITE){
            this.reichweite = MAX_REICHWEITE;
            System.err.println("Maximale Reichweite von "+MAX_REICHWEITE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.reichweite = reichweite;
    }

    @Override
    public void druckeBeschreibung() {
        super.druckeBeschreibung();
        System.out.println("Grabtiefe: "+grabtiefe);
        System.out.println("Reichweite: "+reichweite);
    }
}
