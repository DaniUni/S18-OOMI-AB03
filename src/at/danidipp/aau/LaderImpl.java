package at.danidipp.aau;

public class LaderImpl extends BaumaschineImpl implements Lader{
    public double schaufelvolumen;
    public double kipphoehe;

    public LaderImpl(String name, int leistung, double geschwindigkeit, double gewicht, double schaufelvolumen, double kipphoehe) {
        super(name, leistung, geschwindigkeit, gewicht);
        this.schaufelvolumen = schaufelvolumen;
        this.kipphoehe = kipphoehe;
    }

    @Override
    public double getSchaufelvolumen() {
        return schaufelvolumen;
    }

    @Override
    public void setSchaufelvolumen(double schaufelvolumen) {
        if (schaufelvolumen > MAX_SCHAUFELVOLUMEN){
            this.schaufelvolumen = MAX_SCHAUFELVOLUMEN;
            System.err.println("Maximales Schaufelvolumen von "+MAX_SCHAUFELVOLUMEN+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.schaufelvolumen = schaufelvolumen;
    }

    @Override
    public double getKipphoehe() {
        return kipphoehe;
    }

    @Override
    public void setKipphoehe(double kipphoehe) {
        if (kipphoehe > MAX_KIPPHOEHE){
            this.kipphoehe = MAX_KIPPHOEHE;
            System.err.println("Maximale Kipphöhe von "+MAX_KIPPHOEHE+" überschritten! Aktueller Wert wurde auf den Maximalwert zurückgesetzt.");
        }
        else this.kipphoehe = kipphoehe;
    }

    @Override
    public void druckeBeschreibung() {
        super.druckeBeschreibung();
        System.out.println("Schaufelvolumen: "+schaufelvolumen);
        System.out.println("Kipphoehe: "+kipphoehe);
    }
}
